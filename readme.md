# <h1 align = "center">简易串口通信+多线程示例</h1>

天津恒度byHZ@2020-1-24 

## 介绍

用于快速实现串口通信和多线程的相关功能

## 环境

WIN10+VS2013

## 运行方法

打开工（.sln文件），直接运行main.cpp。串口通信功能需要连接串口设备或使用虚拟串口，观察通信及多线程运行过程。

![image-20210124221845968](img/image-20210124221845968.png)

## 参考资料

- [C++并发操作（多线程）](https://www.cnblogs.com/yskn/p/9355556.html)
- [C++标准库（第二版）](https://www.jb51.net/books/527829.html)
- [C++串口通信](https://download.csdn.net/download/huangzhe0701/14504840)

